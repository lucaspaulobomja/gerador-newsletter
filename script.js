function getData() {
    $.ajax({
        method: "GET",
        // async: true,
        url: "https://seminovos.com.br/noticias/",
        success: data =>{
            $data = $(data);
            var news1 = $($data.find("article.type-post")[0]).find("a");
            news1 = news1.attr("href");

            var news2 = $($data.find("article.type-post")[1]).find("a");
            news2 = news2.attr("href");

            var news3 = $($data.find("article.type-post")[2]).find("a");
            news3 = news3.attr("href");

            var news4 = $($data.find("article.type-post")[3]).find("a");
            news4 = news4.attr("href");
            $.ajax({
                method: "GET",
                url: news1,
                success: data =>{
                    $data = $(data);
                    var news1Photo = ($data.find("div.post-header"));
                    news1Photo = news1Photo.attr("data-src");

                    var news1Title = ($data.find("div.post-header"));
                    news1Title = news1Title.attr("title");

                    var news1Text = ($data.find("div.entry-content"));
                    news1Text = news1Text.find("p").text();

                    var newPhoto1 = $("#firstImageNews");
                    newPhoto1 = newPhoto1.css("background-image", "url(" + news1Photo + ")");
                    
                    var newTitle1 = $("#firstTitleNews");
                    newTitle1 = newTitle1.text(news1Title);

                    var newText1 = $("#firstTextNews");
                    newText1 = newText1.text(news1Text.slice(0, 181) + " ...");

                    var linkNoticiaFoto1 = $("#linkNoticiaFoto1");
                    linkNoticiaFoto1 = linkNoticiaFoto1.attr("href", news1);

                    var linkNoticiaTitulo1 = $("#linkNoticiaTitulo1");
                    linkNoticiaTitulo1 = linkNoticiaTitulo1.attr("href", news1);
                    
                    var linkNoticiaTexto1 = $("#linkNoticiaTexto1");
                    linkNoticiaTexto1 = linkNoticiaTexto1.attr("href", news1);

                    var newLeiaMais1 = $("#leiaMais1");
                    newLeiaMais1 = newLeiaMais1.attr("href", news1);
                }
            });
            $.ajax({
                method: "GET",
                url: news2,
                success: data =>{
                    $data = $(data);
                    var news2Photo = ($data.find("div.post-header"));
                    news2Photo = news2Photo.attr("data-src");

                    var news2Title = ($data.find("div.post-header"));
                    news2Title = news2Title.attr("title");

                    var news2Text = ($data.find("div.entry-content"));
                    news2Text = news2Text.find("p").text();

                    var newPhoto2 = $("#secondImageNews");
                    newPhoto2 = newPhoto2.css("background-image", "url(" + news2Photo + ")");
                    
                    var newTitle2 = $("#secondTitleNews");
                    newTitle2 = newTitle2.text(news2Title);

                    var newText2 = $("#secondTextNews");
                    newText2 = newText2.text(news2Text.slice(0, 181) + " ...");

                    var linkNoticiaFoto2 = $("#linkNoticiaFoto2");
                    linkNoticiaFoto2 = linkNoticiaFoto2.attr("href", news2);

                    var linkNoticiaTitulo2 = $("#linkNoticiaTitulo2");
                    linkNoticiaTitulo2 = linkNoticiaTitulo2.attr("href", news2);
                    
                    var linkNoticiaTexto2 = $("#linkNoticiaTexto2");
                    linkNoticiaTexto2 = linkNoticiaTexto2.attr("href", news2);
                    
                    var newLeiaMais2 = $("#leiaMais2");
                    newLeiaMais2 = newLeiaMais2.attr("href", news2);
                }
            });
            $.ajax({
                method: "GET",
                url: news3,
                success: data =>{
                    $data = $(data);
                    var news3Photo = ($data.find("div.post-header"));
                    news3Photo = news3Photo.attr("data-src");

                    var news3Title = ($data.find("div.post-header"));
                    news3Title = news3Title.attr("title");

                    var news3Text = ($data.find("div.entry-content"));
                    news3Text = news3Text.find("p").text();

                    var newPhoto3 = $("#thirdImageNews");
                    newPhoto3 = newPhoto3.css("background-image", "url(" + news3Photo + ")");
                    
                    var newTitle3 = $("#thirdTitleNews");
                    newTitle3 = newTitle3.text(news3Title);

                    var newText3 = $("#thirdTextNews");
                    newText3 = newText3.text(news3Text.slice(0, 181) + " ...");

                    var linkNoticiaFoto3 = $("#linkNoticiaFoto3");
                    linkNoticiaFoto3 = linkNoticiaFoto3.attr("href", news3);

                    var linkNoticiaTitulo3 = $("#linkNoticiaTitulo3");
                    linkNoticiaTitulo3 = linkNoticiaTitulo3.attr("href", news3);
                    
                    var linkNoticiaTexto3 = $("#linkNoticiaTexto3");
                    linkNoticiaTexto3 = linkNoticiaTexto3.attr("href", news3);

                    var newLeiaMais3 = $("#leiaMais3");
                    newLeiaMais3 = newLeiaMais3.attr("href", news3);
                }
            });
            $.ajax({
                method: "GET",
                url: news4,
                success: data =>{
                    $data = $(data);
                    var news4Photo = ($data.find("div.post-header"));
                    news4Photo = news4Photo.attr("data-src");

                    var news4Title = ($data.find("div.post-header"));
                    news4Title = news4Title.attr("title");

                    var news4Text = ($data.find("div.entry-content"));
                    news4Text = news4Text.find("p").text();

                    var newPhoto4 = $("#fourthImageNews");
                    newPhoto4 = newPhoto4.css("background-image", "url(" + news4Photo + ")");
                    
                    var newTitle4 = $("#fourthTitleNews");
                    newTitle4 = newTitle4.text(news4Title);

                    var newText4 = $("#fourthTextNews");
                    newText4 = newText4.text(news4Text.slice(0, 181) + " ...");

                    var linkNoticiaFoto4 = $("#linkNoticiaFoto4");
                    linkNoticiaFoto4 = linkNoticiaFoto4.attr("href", news4);

                    var linkNoticiaTitulo4 = $("#linkNoticiaTitulo4");
                    linkNoticiaTitulo4 = linkNoticiaTitulo4.attr("href", news4);
                    
                    var linkNoticiaTexto4 = $("#linkNoticiaTexto4");
                    linkNoticiaTexto4 = linkNoticiaTexto4.attr("href", news4);
                    
                    var newLeiaMais4 = $("#leiaMais4");
                    newLeiaMais4 = newLeiaMais4.attr("href", news4);
                }
            });
        }
    });
    // $.ajax({
    //     method: "GET",
    //     url: "https://seminovos.com.br",
    //     success: data =>{
    //         $data = $(data);

    //         var random1 = Math.floor(Math.random() * 10) + 1;
    //         var random2 = Math.floor(Math.random() * 10) + 1;
    //         var random3 = Math.floor(Math.random() * 10) + 1;
    //         do{
    //             random1;
    //             random2;
    //             random3;
    //         } while (   random1 == random2 ||
    //                     random2 == random3 ||
    //                     random3 == random1);
    //         console.log(random1, random2, random3);
    //         }
    // });
};
getData();